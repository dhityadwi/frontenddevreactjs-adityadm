import { BrowserRouter, Route } from "react-router-dom";
import "./App.css";
import Homepage from "./component/home/Homepage";

function App() {
  return (
    <BrowserRouter>
      <Route path="" component={Homepage} exact />
    </BrowserRouter>
  );
}

export default App;
