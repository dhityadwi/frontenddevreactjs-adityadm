import React from "react";
import Main from "../pages/Main/Main";
import Header from "../header/Topheader";

const Homepage = () => {
  return (
    <div>
      <Header />
      <Main />
    </div>
  );
};

export default Homepage;
