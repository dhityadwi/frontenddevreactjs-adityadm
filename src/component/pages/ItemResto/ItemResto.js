import React from "react";
import { Link } from "react-router-dom";
import { genres, IMG_API } from "../../../service/listService";

const ItemResto = ({ title, poster_path, genre, id }) => {
  return (
    <div className="card" style={{ cursor: "pointer" }}>
      <Link
        to={`detail/${id}`}
        style={{ color: "black", textDecoration: "none" }}
      >
        <img src={IMG_API + poster_path} alt="" />
        <label style={{ fontSize: "15px" }}>{title}</label>
        {genres.map((item) =>
          genre === item.id ? <p key={item.id}>{item.name}</p> : null
        )}
      </Link>
      <div
        style={{
          background: "blue",
          color: "white",
          textAlign: "center",
          height: "50px",
          lineHeight: "40px",
        }}
      >
        More Detail
      </div>
    </div>
  );
};

export default ItemResto;
