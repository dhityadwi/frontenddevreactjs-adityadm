import React, { useEffect, useState } from "react";
import "./Main.css";

import ItemResto from "../ItemResto/ItemResto";
import { GET_ALL_RESTO, GET_RESTOS_BY_ID } from "../../../service/listService";
import axios from "axios";
import Paginations from "../../pagination/Paginations";

const Main = () => {
  const [restos, setRestos] = useState([]);
  const [data, setData] = useState([]);
  const [page, setPage] = useState(0);
  const [price, setPrice] = useState([
    "Price",
    "$10",
    "$20",
    "$30",
    "$40",
    "$50",
    "$60",
    "$70",
    "$80",
    "$90",
  ]);
  const [cat, setCat] = useState([
    "Category",
    "Sweet",
    "Fast Food",
    "Snack",
    "Rice",
    "Bakery",
    "Beverages",
  ]);

  const fetchRestosById = async (id) => {
    const result = await axios.get(`${GET_RESTOS_BY_ID()}`);
    console.log(result);
  };

  const getRestos = async (url) => {
    const res = await axios.get(url);

    setData(res.data.results);
    setRestos(res.data.results);
  };

  const getAllRestos = () => {
    setRestos(data);
  };

  useEffect(() => {
    getRestos(GET_ALL_RESTO);
    fetchRestosById();
    getRestos(page);
  }, [page]);

  return (
    <>
      <div>
        <div className="id">
          <div>Filter By:</div>
          <div className="radio-buttons">
            <input name="platform" type="radio" />
            Open Now
          </div>
          <div className="price">
            <select>
              {price.map((type) => (
                <option>{type}</option>
              ))}
            </select>
          </div>
          <div className="category">
            <select>
              {cat.map((type) => (
                <option>{type}</option>
              ))}
            </select>
          </div>
          <div className="tbl">
            <div className="hapus">Clear All</div>
          </div>
        </div>
      </div>
      <div className="home">
        <strong>All Restaurant</strong>
        <div className="card-container">
          {restos.length > 0 &&
            restos.map((restor) => (
              <ItemResto
                key={restor.id}
                title={restor.title}
                genre={restor.genre_ids[0]}
                poster_path={restor.poster_path}
                id={restos.id}
              />
            ))}
        </div>
        <div
          style={{
            width: "100%",
            display: "flex",
            justifyContent: "center",
            marginTop: 10,
          }}
        ></div>
      </div>
    </>
  );
};

export default Main;
