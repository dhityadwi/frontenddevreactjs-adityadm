import React from "react";
import "./Topheader.css";

const Topheader = () => {
  return (
    <div className="head">
      <h1>Restaurant</h1>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam aliquam,
        sem id luctus congue, nunc felis scelerisque nulla, at molestie tellus
        nibh et eros. Vivamus posuere, ex et dictum tristique, velit diam
        porttitor ex, ut euismod nunc tortor sed dui. Morbi at tellus et est
        pretium mattis. Praesent condimentum sit.{" "}
      </p>
    </div>
  );
};

export default Topheader;
